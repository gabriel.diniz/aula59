public class aula59 {  // função matricial

    public static void main(String[] args) {
        int[][] valores = new int[10][10]; // array 2 dimensões valores, nova instancia, cada dimensão 10 elementos
            // laço de repetição
        for (int x = 0; x < 10; x++) { // variavel inteira x igual a 0, repetir enquanto x menor do que 10, cada ciclo acrescenta uma unidade
            String str = "";  // variavel string vazia
            for (int y = 0; y < 10; y++) {  // variavel inteira x igual a 0, repetir enquanto y menor do que 10, cada ciclo acrescenta uma unidade
                valores[x][y] = (3 * (x + 1) + (y * y)); // array com resultados

                if (y < 9) // se y menor que 9
                    str += valores[x][y] + ", "; // variavel str, recebe valor x y + ,
                else // se não
                    str += valores[x][y]; // recebe valor x y
            }
            System.out.println(str); // imprime valor da variavel str
        }
    }
}